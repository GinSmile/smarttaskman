package cn.edu.neuq.smarttaskman;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class about_info extends Activity{
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.about);
	    Button share = (Button)findViewById(R.id.share);
	    share.setOnClickListener(new OnClickListener()
		{
			public void onClick(View source)
			{
				Intent intent=new Intent(Intent.ACTION_SEND);  
				intent.setType("text/plain");  //纯文本
				intent.putExtra(Intent.EXTRA_SUBJECT, "分享");  
				intent.putExtra(Intent.EXTRA_TEXT, "我发现了一个好用的Android APP，SmartTaskMan。");  
				startActivity(intent);  								
			}
		});		
	}
}
