package cn.edu.neuq.smarttaskman;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Debug;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleAdapter.ViewBinder;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private static final int EXIT = 0x113;
	private static final int ABOUT = 0x114;
	private static final int INFO = 0x115;
	private static final int SHARE = 0x116;	
	private static final int TASK = 0x117;
	private static final int SERVICE = 0x118;	
	
	private TextView proNum;  //总的进程数量
	private TextView leftMem;  //剩余内存
	private ListView proList;
	private String leftMemSize;//String类型的剩余内存	
	private ArrayList<String> arrayListPro;
	List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
	
	public ActivityManager myActivityManager; 
	public List<ActivityManager.RunningAppProcessInfo> mRunningPros;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);		
		proNum = (TextView)findViewById(R.id.proNum);
		leftMem = (TextView)findViewById(R.id.leftMem); 
		proList = (ListView)findViewById(R.id.proList);
		myActivityManager =(ActivityManager)getSystemService(Activity.ACTIVITY_SERVICE);
		upDateMemInfo();
		Button refresh = (Button)findViewById(R.id.refresh);
		Button kill_all = (Button)findViewById(R.id.kill_all);	
		
		
		//获取数据，以ListView显示出来
		getRunningProcessInfo();
		
		/****************绑定监听器的工作*********************************************/
		
		//为每一个ListView项绑定监听器，点击时触发
		proList.setOnItemClickListener(new OnItemClickListener(){
			 public void onItemClick(AdapterView<?> arg0, View arg1,  final int position, long arg3) {   
				 new AlertDialog.Builder(MainActivity.this).setMessage("是否杀死该进程")
				    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {							
							String processName = mRunningPros.get(position).processName;
							myActivityManager.killBackgroundProcesses(processName);
							getRunningProcessInfo() ;
							upDateMemInfo();
							makeToastSimple("已杀死进程"	+ processName, true);
						}
					}).setNegativeButton("取消", new DialogInterface.OnClickListener() {						
						@Override
						public void onClick(DialogInterface dialog, int which) {							
							dialog.cancel() ;
						}
					}).create().show() ;
	               
	           }   
		});
		
		//为每一个ListView项绑定监听器，长按时触发
		proList.setOnItemLongClickListener(new ListView.OnItemLongClickListener(){
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, final View view,
					final int position, long arg3) {
				final String processName = mRunningPros.get(position).processName;
				android.content.DialogInterface.OnClickListener listener1 = new DialogInterface.OnClickListener(){
					public void onClick(DialogInterface dialog, int which) {
						if(which==0){
							myActivityManager.killBackgroundProcesses(processName);
							getRunningProcessInfo() ;
							upDateMemInfo();
							makeToastSimple("已杀死进程"	+ processName, true);							
						}else if (which==1){							
							check_detail(position);
						}
					}
				};
				new AlertDialog.Builder(MainActivity.this)
					.setTitle("操作")
					.setItems(R.array.operation,listener1)
					.setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener(){
						//取消
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel() ;
						}
					}).show();
				return true;
			}
        });				
		
		//为主Activity的两个Button绑定监听器
		kill_all.setOnClickListener(new OnClickListener()
		{
			public void onClick(View source)
			{
				for (ActivityManager.RunningAppProcessInfo amPro : mRunningPros){
					String processName = amPro.processName;
					myActivityManager.killBackgroundProcesses(processName);
				}
				getRunningProcessInfo();
				//更新内存信息
				upDateMemInfo();
				makeToastSimple("一键清理结束，当前可用内存为" + leftMemSize, true);								
			}
		});		
		refresh.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View source)
			{
				getRunningProcessInfo();
				makeToast("已刷新");
				//更新内存信息
				upDateMemInfo();				
			}
		});	
			
	}	
	
	//获得正在运行进程的某些信息
	public void getRunningProcessInfo(){
	    myActivityManager = (ActivityManager)MainActivity.this.getSystemService(ACTIVITY_SERVICE); 
	    mRunningPros = myActivityManager.getRunningAppProcesses();
	    proNum.setText(mRunningPros.size()+"");
		//arrayListPro = new ArrayList<String>(); 
	    listItems.clear();
		
		
		PackagesInfo pi = new PackagesInfo(this);  	
		for (ActivityManager.RunningAppProcessInfo amPro : mRunningPros){		
			
//			//过滤系统的应用和电话应用
//			 if(amPro.processName.equals("system") || amPro.processName.equals("com.Android.phone")){ 
//	                continue; 
//	            }
			
			// 获得该进程占用的内存
			int[] myMempid = new int[] {amPro.pid};
			// 此MemoryInfo位于android.os.Debug.MemoryInfo包中，用来统计进程的内存信息
			Debug.MemoryInfo[] memoryInfo = myActivityManager.getProcessMemoryInfo(myMempid);			
			// 获取进程占内存信息形如3.14MB
			double memSize = memoryInfo[0].dalvikPrivateDirty/1024.0;	
			int temp = (int)(memSize*100);
			memSize = temp/100.0;
			
			String ProInfo="";
			ProInfo +="Name:"+ amPro.processName
					+ "\nID:" + amPro.pid 
					+ "\nMemory:" + memSize + "MB";
			

			Drawable myIcon = null;
			try{
				//Log.v("ccc", pi.getInfo(amPro.processName).packageName);
				myIcon = pi.getInfo(amPro.processName).loadIcon(this.getPackageManager());
			}catch(Exception e){
				
			}
			
			
			Map<String, Object> listItem = new HashMap<String, Object>();
			if(myIcon == null) listItem.put("header", R.drawable.def);
			else listItem.put("header", myIcon);
			listItem.put("info", ProInfo);
			listItems.add(listItem);
		}


		SimpleAdapter simpleAdapter = new SimpleAdapter(this, 
				listItems, 
				R.layout.item,
				new String[]{"info", "header"},
				new int[]{R.id.info,R.id.header});
		
		//使用SimpleAdapter的ViewBinder方法来实现对Drawable或者Image类型图片的更新
		proList = (ListView)findViewById(R.id.proList);
		proList.setAdapter(simpleAdapter);
		simpleAdapter.setViewBinder(new ViewBinder() {
			public boolean setViewValue(View view, Object data,
					String textRepresentation) {
				if (view instanceof ImageView && data instanceof Drawable) {
					ImageView iv = (ImageView) view;
					iv.setImageDrawable((Drawable) data);
					return true;
				} else
					return false;
			}
		});

		
		
		
		
		

  }
	
	//更新可用内存信息
    public void upDateMemInfo(){	
    	//获得MemoryInfo对象  
    	ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        //获得系统可用内存，保存在MemoryInfo对象上  
        myActivityManager.getMemoryInfo(memoryInfo) ;  
        long memSize = memoryInfo.availMem ;            
        //字符类型转换 
        leftMemSize = Formatter.formatFileSize(getBaseContext(), memSize);
        leftMem.setText(leftMemSize);
	}
    
    //一个带图片的toast
    public void makeToast(String str){
    	Toast toast = Toast.makeText(MainActivity.this,str, Toast.LENGTH_SHORT);
		View toastView = toast.getView();
		ImageView image = new ImageView(MainActivity.this);
		image.setImageResource(R.drawable.ic_launcher);
		LinearLayout ll = new LinearLayout(MainActivity.this);
		ll.addView(image);
		ll.addView(toastView);
		toast.setView(ll);						
		toast.show();
    }
    
  //简单的toast
    public void makeToastSimple(String str,boolean sel){
    	Toast toast = Toast.makeText(MainActivity.this,str, 
    			sel?Toast.LENGTH_SHORT:Toast.LENGTH_LONG);	
		toast.show();
    }
       
	public void check_detail(int position){
		ActivityManager.RunningAppProcessInfo processInfo =  mRunningPros.get(position);							
		Intent intent = new Intent(this, detail.class);
		intent.putExtra("EXTRA_PROCESS_PID", processInfo.pid + "");
	    intent.putExtra("EXTRA_PROCESS_UID", processInfo.uid + "");
	    intent.putExtra("EXTRA_PROCESS_NAME", processInfo.processName);	
	    intent.putExtra("EXTRA_PROCESS_IMPORTANCE", processInfo.importance + "");
	    intent.putExtra("EXTRA_PROCESS_IMPORTANCE_REASON_CODE", processInfo.importanceReasonCode + "");	
	    intent.putExtra("EXTRA_PROCESS_IMPORTANCE_REASON_PID", processInfo.importanceReasonPid + "");	    
	    intent.putExtra("EXTRA_PROCESS_LRU", processInfo.lru + "");	
	    intent.putExtra("EXTRA_PKGNAMELIST", processInfo.pkgList) ;
	    startActivity(intent);
	}
	
	//以下是按下menu呼出的菜单的添加处理
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {	
		menu.add(0, TASK, 0, "查看正在运行的任务");
		menu.add(0, SERVICE, 0, "查看正在运行的服务");
		menu.add(0, ABOUT, 0, "关于应用");
		menu.add(0, INFO, 0, "系统信息");
		menu.add(0, SHARE, 0, "分享应用");
		menu.add(0, EXIT, 0, "退出应用");
		return super.onCreateOptionsMenu(menu);
	}
	
	public void show_tasks(){
		Intent intent = new Intent(this, getTask.class);
		startActivity(intent);
	}
	
	public void show_services(){
		Intent intent = new Intent(this, getService.class);
		startActivity(intent);
	}
	
	public void about_info(){
		Intent intent = new Intent(this, about_info.class);
		startActivity(intent);
	}
	
	public void system_info(){
		Intent intent = new Intent(this, system_info.class);
		startActivity(intent);
	}	
	
	public void share(){
		Intent intent=new Intent(Intent.ACTION_SEND);  
		intent.setType("text/plain");  //纯文本
		intent.putExtra(Intent.EXTRA_SUBJECT, "分享");  
		intent.putExtra(Intent.EXTRA_TEXT, "我发现了一个好用的Android APP，SmartTaskMan。");  
		startActivity(intent);  
	}
	
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem mi){
		switch (mi.getItemId()){
			case TASK:
				show_tasks();
				break;
			case SERVICE:
				show_services();
				break;
			case ABOUT:
				about_info();
				break;
			case INFO:
				system_info();
				break;
			case SHARE:
				share();
				break;
			case EXIT:
				finish();
				break;
		}
		return true;
	}
	
	
}
