package cn.edu.neuq.smarttaskman;

import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.app.ActivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class getTask extends Activity  
{  
  private Button refresh;  
  private ListView proList;  
  private ArrayAdapter<String> arrayAdapter;  
  private ArrayList<String> arrayListPro;  
  private ActivityManager myActivityManager;  
  private final int maxNum = 40;

  @Override  
  public void onCreate(Bundle savedInstanceState)  
  {  
    super.onCreate(savedInstanceState);  
    setContentView(R.layout.list);   
    refresh = (Button)findViewById(R.id.Refresh);  
    proList = (ListView)findViewById(R.id.List);  
    
    //获取正在运行的进程的信息并在ListView中显示出来
    getRunningTaskInfo();
    
    //为refresh按钮绑定监听器
    refresh.setOnClickListener(new Button.OnClickListener()  
    {  
      @Override  
      public void onClick(View v)  
      {  
    	  getRunningTaskInfo();
    	  Toast toast = Toast.makeText(getTask.this,"已刷新", Toast.LENGTH_SHORT);
		  toast.show();
      }  
    });  

  }
  
  public void getRunningTaskInfo(){
	    //getSystemService(ACTIVITY_SERVICE)的到系统的服务信息，数据类型是ActivityManager
	    myActivityManager = (ActivityManager)getTask.this.getSystemService(ACTIVITY_SERVICE);  
		//新建一个String类型的ArrayList，用来存放数据
	    arrayListPro = new ArrayList<String>(); 
		List<ActivityManager.RunningTaskInfo> mRunningTasks = myActivityManager.getRunningTasks(maxNum);
		//顺序枚举每个元素，使用ArrayList<String>类型的add方法添加元素
		for (ActivityManager.RunningTaskInfo amTask : mRunningTasks)
			arrayListPro.add(amTask.baseActivity.getClassName()+ "(ID=" + amTask.id +")"); 
		arrayAdapter = new ArrayAdapter<String> (getTask.this, android.R.layout.simple_list_item_1, arrayListPro); 
		//用setAdapter()将ListView和Adapter绑定
		proList.setAdapter(arrayAdapter);
  }  
} 