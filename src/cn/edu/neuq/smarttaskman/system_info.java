package cn.edu.neuq.smarttaskman;

import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class system_info extends Activity {

    private ListView mLisview;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.system);        
        String[][] systemInfo = getSystemInfo();        
        MyAdapter myAdapter = new MyAdapter(getApplicationContext(), systemInfo);
        mLisview = (ListView)findViewById(R.id.sysList);
        mLisview.setAdapter(myAdapter);        
    }
	
	private class MyAdapter extends BaseAdapter{		
		private String[][] systemInfo;		
		
		MyAdapter(Context context,String[][] systemInfo){
			this.systemInfo = systemInfo;
		}
		
		@Override
		public int getCount() {
			return systemInfo.length;
		}

		@Override
		public Object getItem(int position) {
			return systemInfo[position];
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			convertView = getLayoutInflater().inflate(R.layout.system_item, null);
			TextView name = (TextView) convertView.findViewById(R.id.name);
			TextView value = (TextView) convertView.findViewById(R.id.value);			
			name.setText(systemInfo[position][1]);
			value.setText(systemInfo[position][0]);			
			return convertView;
		}		
	}

	private String[][] getSystemInfo() {
    	Build mBuild = new Build();
    	String[][] systemInfo = {
    			{mBuild.BOARD,"主板"},
    			{mBuild.BRAND,"系统定制商   "},
    			{mBuild.CPU_ABI,"cpu指令集 "},
    			{mBuild.DEVICE,"设备参数    "},
    			{mBuild.DISPLAY," 显示屏参数  "},
    			{mBuild.FINGERPRINT,"硬件名称"},
    			{mBuild.HOST,"主机   "},
    			{mBuild.MANUFACTURER," 硬件制造商    "},
    			{mBuild.MODEL,"版本   "},
    			{mBuild.PRODUCT," 手机制造商  "},
    			{Build.VERSION.RELEASE,"版本字符串格式  "}
    			};
		return systemInfo;
	}
    
}
